﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        public Task<List<T>> GetAllAsync() => Task.FromResult(Data);

        public Task<T> GetByIdAsync(Guid id) => Task.FromResult(Data.FirstOrDefault(x => x.Id == id));

        public Task<bool> CreateAsync(T entity)
        {
            Data.Add(entity);
            return Task.FromResult(true);
        }

        public Task<T> UpdateAsync(T entity)
        {
            var index = Data.FindIndex(item => item.Id == entity.Id);

            Data[index] = entity;

            return Task.FromResult(entity);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            if (!Data.Any(e => e.Id == id))
                return Task.FromResult(false);
            
            var result = Data.RemoveAll(e => e.Id == id);

            return Task.FromResult(result > 0);
        }
    }
}